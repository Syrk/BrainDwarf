﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notes.Data
{
    /// <summary>
    /// Note class containing the relevant information for the notes.
    /// </summary>
    internal class Note
    {
        #region fields
        private static int id;
        private int _actualID;
        private string _message;
        private DateTime _expirationDate;
        private Discord _discordInfo;

        /// <summary>
        /// Returns the unique id for the note.
        /// </summary>
        public int ID
        {
            get
            {
                return _actualID;
            }
        }

        /// <summary>
        /// Returns the message for the note.
        /// </summary>
        public string Message
        {
            get
            {
                string message = string.Format(
                   $"@{_discordInfo.UserID}\n" +
                   $"Date: {_expirationDate}\n" +
                   _message);

                return message;
            }
        }

        /// <summary>
        /// The date where the note shall expire.
        /// </summary>
        public DateTime ExpirationDate
        {
            get
            {
                return _expirationDate;
            }
        }

        /// <summary>
        /// Returns the properties according to Discord.
        /// </summary>
        public Discord DiscordInfo
        {
            get
            {
                return _discordInfo;
            }
        }
        #endregion

        #region ctor
        public Note(string message, DateTime expirationDate, Discord discordInfo)
        {
            _message = message;
            _expirationDate = expirationDate;
            _discordInfo = discordInfo;
            _actualID = id;

            id++;
        }
        #endregion
    }
}